package io.github.mreditor.yamoviedb.ui.board

import io.github.mreditor.yamoviedb.R
import io.github.mreditor.yamoviedb.data.sources.AuthSource
import io.github.mreditor.yamoviedb.data.sources.FilmsRepository

class StarredFragment : FilmsFragment(R.layout.fragment_starred) {
    override fun createAdapter() =
        FilmCardAdapter(
            this,
            user.stars.map(films::get).toMutableList(),
            iterator { }
        )
}