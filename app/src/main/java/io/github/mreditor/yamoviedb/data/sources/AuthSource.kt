package io.github.mreditor.yamoviedb.data.sources

import android.app.Activity
import io.github.mreditor.yamoviedb.data.models.User
import io.github.mreditor.yamoviedb.ui.board.BoardActivity
import java.security.MessageDigest

class AuthSource(private val activity: Activity) {
    private val db = DbAdapter(activity, User::class)
    private val users = db.fetchAll().use { rows ->
        rows.map { it.apply { stars = StarsSet(activity, it.username) } }.toMutableList()
    }

    enum class Status { Ok, NoUser, WrongPassword }

    fun login(username: String, password: String) =
        when (
            users.firstOrNull { it.username == username }
                ?.let { it.passwordHash == passwordHash(password) }
            ) {
            true -> Status.Ok
            false -> Status.WrongPassword
            null -> Status.NoUser
        }

    fun register(username: String, password: String) {
        val user = User(username, passwordHash(password)).apply {
            stars = StarsSet(activity, username)
        }
        users.add(user)
        db.insert(user)
    }

    fun getCurrentUser() = getUser(
        activity.intent.extras!!.getString(BoardActivity.USERNAME)!!,
        activity.intent.extras!!.getString(BoardActivity.PWHASH)!!
    )

    fun getUser(username: String, passwordHash: String) =
        users.first { it.username == username && it.passwordHash == passwordHash }

    fun passwordHash(password: String) = String(
        MessageDigest.getInstance("SHA-1").digest(password.toByteArray())
    )
}
