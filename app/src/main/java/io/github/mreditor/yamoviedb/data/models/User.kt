package io.github.mreditor.yamoviedb.data.models

import io.github.mreditor.yamoviedb.data.sources.StarsSet

data class User(val username: String, val passwordHash: String) {
    lateinit var stars: StarsSet
}