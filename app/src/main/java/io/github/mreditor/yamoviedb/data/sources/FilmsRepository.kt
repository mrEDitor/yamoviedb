package io.github.mreditor.yamoviedb.data.sources

import android.app.Activity
import android.util.Log
import android.widget.Toast
import io.github.mreditor.yamoviedb.data.models.Film
import org.jsoup.Jsoup
import java.io.File
import java.lang.Exception
import java.lang.Integer.max
import java.net.URL

class FilmsRepository(private val activity: Activity) {
    private val db = DbAdapter(activity, Film::class)
    private val items = db.fetchAll().use { rows ->
        rows.associateBy { it.id }.toMutableMap()
    }

    fun get(id: Int) = items[id]
        ?: throw NoSuchElementException()

    fun list(atLeast: Int = 0) = sequence {
        yieldAll(items.values)
        val fetchMore = max(0, atLeast - items.count())
        yieldAll(fetch().take(fetchMore).filterNotNull())
    }

    fun fetch() = sequence {
        var cooldown = 0
        for (i in generateSequence(1) { it + 1 }) {
            try {
                val soup = Jsoup.connect("https://kinobaza.com/page/$i/").get()
                for (item in soup.select("article.movie-item").toTypedArray()) {
                    try {
                        val id = item.select(".movie-title > a").attr("href")
                            .substring("https://kinobaza.com/".length).substringBefore('-').toInt()
                        val img = item.select("img").attr("src")
                        File(activity.cacheDir, "$id.jpg").apply {
                            createNewFile()
                            writeBytes(URL("https://kinobaza.com$img").readBytes())
                        }
                        val labels = item.select("ul.movie-lines > li")
                            .eachText()
                            .associate {
                                it.substringBefore(':').trim() to it.substringAfter(':').trim()
                            }
                        val film = Film(
                            id,
                            item.select(".movie-title > a").text(),
                            labels.getOrDefault("Жанр", ""),
                            labels.getOrDefault("Год выпуска", ""),
                            item.select(".movie-desc").text(),
                            labels.getOrDefault("В ролях", "")
                        )
                        if (!items.containsKey(film.id)) {
                            items[film.id] = film
                            db.insert(film)
                            yield(film)
                        }
                    } catch (e: Exception) {
                        Log.e("kinobaza", "Unable to parse film card", e)
                        Log.d("kinobaza", "HTML was ${item.html()}")
                    }
                }
                cooldown = 0
            } catch (e: Exception) {
                Log.e("kinobaza", "No internet?", e)
                activity.runOnUiThread {
                    Toast.makeText(
                        activity,
                        e.localizedMessage,
                        Toast.LENGTH_LONG
                    ).show()
                }
                yield(null)
                Thread.sleep(++cooldown * 500L)
            }
        }
    }
}