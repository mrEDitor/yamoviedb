package io.github.mreditor.yamoviedb.data.models

data class Film(
    val id: Int,
    val title: String,
    val genre: String,
    val year: String,
    val description: String,
    val starring: String
)